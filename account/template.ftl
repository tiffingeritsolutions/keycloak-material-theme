<#macro mainLayout active bodyClass>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="robots" content="noindex, nofollow">

    <title>${msg("accountManagementTitle",(realm.displayName!''))}</title>
    <link rel="icon" href="${url.resourcesPath}/img/favicon.ico">
    <#if properties.styles?has_content>
        <#list properties.styles?split(' ') as style>
            <link href="${url.resourcesPath}/${style}" rel="stylesheet" />
        </#list>
    </#if>
    <#if properties.scripts?has_content>
        <#list properties.scripts?split(' ') as script>
            <script type="text/javascript" src="${url.resourcesPath}/${script}"></script>
        </#list>
    </#if>
</head>
<body class="admin-console user ${bodyClass}">
        
    <header class="">
        <nav class="navbar navbar-expand-lg navbar-dark primary-color" role="navigation">
                <div class="container">
            <div class="navbar-brand">
                ${msg("accountManagementTitle",(realm.displayName!''))}
            </div>
            <div class="collapse navbar-collapse">
                    <ul class="navbar-nav ml-auto">
                        <#if realm.internationalizationEnabled>
                            <li class="nav-item dropdown" id="kc-locale-dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="kc-current-locale-link">${locale.current}</a>
                                    <div class="dropdown-menu dropdown-primary">
                                        <#list locale.supported as l>
                                            <a class="dropdown-item" href="${l.url}">${l.label}</a>
                                        </#list>
                                    </div>
                            <li>
                        </#if>
                        <#if referrer?has_content && referrer.url?has_content><li class="nav-item"><a class="nav-link" href="${referrer.url}" id="referrer">${msg("backTo",referrer.name)}</a></li></#if>
                        <li class="nav-item"><a class="nav-link" href="${url.logoutUrl}">${msg("doSignOut")}</a></li>
                    </ul>
            </div>
                </div>
        </nav>
    </header>

    <div class="container mt-4">
    <div class="row">
        <div class="bs-sidebar col-sm-3">
            <ul class="nav flex-column nav-pills">
                <li class="nav-item"><a class="nav-link <#if active=='account'>active</#if>" href="${url.accountUrl}">${msg("account")}</a></li>
                <#if features.passwordUpdateSupported><li class="nav-item"><a class="nav-link <#if active=='password'>active</#if>" href="${url.passwordUrl}">${msg("password")}</a></li></#if>
                <li class="nav-item"><a class="nav-link <#if active=='totp'>active</#if>" href="${url.totpUrl}">${msg("authenticator")}</a></li>
                <#if features.identityFederation><li class="nav-item"><a class="nav-link <#if active=='social'>active</#if>" href="${url.socialUrl}">${msg("federatedIdentity")}</a></li></#if>
                <li class="nav-item"><a class="nav-link <#if active=='sessions'>active</#if>" href="${url.sessionsUrl}">${msg("sessions")}</a></li>
<#--                <li class="nav-item"><a class="nav-link <#if active=='applications'>active</#if>" href="${url.applicationsUrl}">${msg("applications")}</a></li>-->
                <#if features.log><li class="nav-item"><a class="nav-link <#if active=='log'>active</#if>" href="${url.logUrl}">${msg("log")}</a></li></#if>
                <#if realm.userManagedAccessAllowed && features.authorization><li class="nav-item"><a class="nav-link <#if active=='authorization'>active</#if>" href="${url.resourceUrl}">${msg("myResources")}</a></li></#if>
            </ul>
        </div>

        <div class="col-sm-9">
          <div class="card">
          <div class="card-body">
            <#if message?has_content>
                <div class="alert alert-${message.type}">
                    <#if message.type=='success' ><span class="pficon pficon-ok"></span></#if>
                    <#if message.type=='error' ><span class="pficon pficon-error-octagon"></span><span class="pficon pficon-error-exclamation"></span></#if>
                    ${kcSanitize(message.summary)?no_esc}
                </div>
            </#if>

            <#nested "content">
          </div>
          </div>
        </div>
    </div>
    </div>

</body>
</html>
</#macro>
