var scriptUrl = document.getElementsByTagName("script")[0].src;

$(function() {
  $('.useless-wrapper').contents().unwrap();
  $('.has-error input').addClass("is-invalid");

  var s = document.createElement("script");
  s.type = "text/javascript";
  s.src = scriptUrl.substr(0, scriptUrl.lastIndexOf('/') + 1) + "mdb.min.js";
  $("head").append(s);
});

