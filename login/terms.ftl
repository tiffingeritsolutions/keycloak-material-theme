<#import "template.ftl" as layout>
<@layout.registrationLayout displayMessage=false displayWide=true; section>
    <#if section = "header">
        ${msg("termsTitle")}
    <#elseif section = "form">
    <script>$('.container > .card').removeClass('col-md-6 col-md-8 col-xl-8 col-lg-10')</script>
    <div id="kc-terms-text">
        ${kcSanitize(msg("termsText"))?no_esc}
        <div class="custom-control custom-checkbox">
            <input type="checkbox" class="custom-control-input" id="dataProtection" onclick="$('#kc-accept').prop('disabled', !$('#dataProtection').is(':checked') || !$('#dsgvo').is(':checked'))">
            <label class="custom-control-label" for="dataProtection">Ich stimme den AGBs und der Datenschutzerklärung zu</label>
        </div>
        <div class="custom-control custom-checkbox">
            <input type="checkbox" class="custom-control-input" id="dsgvo" onclick="$('#kc-accept').prop('disabled', !$('#dataProtection').is(':checked') || !$('#dsgvo').is(':checked'))">
            <label class="custom-control-label" for="dsgvo">Ich willige der Speicherung meiner Daten ein</label>
        </div>
    </div>
    <form class="form-actions text-right" action="${url.loginAction}" method="POST">
        <button class="${properties.kcButtonClass!} ${properties.kcButtonDefaultClass!}" name="cancel" id="kc-decline" type="submit">${msg("doDecline")}</button>
        <button class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!}" name="accept" id="kc-accept" type="submit" disabled="disabled">${msg("doAccept")}</button>
    </form>
    <div class="clearfix"></div>
    </#if>
</@layout.registrationLayout>
